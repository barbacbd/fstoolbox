from setuptools import setup, find_namespace_packages

from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# establish the version for this package.
with open(path.join(this_directory, 'fstoolbox/VERSION'), encoding='utf-8') as f:
    _version = f.read()

setup(
    name='fstoolbox',
    version=_version,
    packages=find_namespace_packages(include='fstoolbox/*'),
    description='Wrapper for the FSToolbox library created in C.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Brent Barbachem',
    author_email='barbacbd@dukes.jmu.edu',
    license='Proprietary',
    include_package_data=True,
    package_data={'': ['VERSION']},
    python_requires='>=3.4, <4',
    install_requires=[
        'futures'
    ],
    dependency_links=[
        'https://pypi.org/simple/'
    ],
    entry_points={
        'console_scripts': [
        ]
    },
    zip_safe=False
)
