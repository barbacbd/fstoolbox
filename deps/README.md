# FS Tool Box

The original package was developed by other parties. The reader/user can unzip the original package and read more about the work.

After the package has been unzipped, the reader/user can install the package locally via:

`
cd FEAST-v2.0.0_1;
cd MIToolbox;
make && sudo make install;
cd ../FEAST;
make && sudo make install;
`

The user should now have the libFSToolbox.so installed.