from ctypes import cdll, POINTER, c_uint, c_double
from os import path

# Report the Version for this project
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'VERSION'), encoding='utf-8') as f:
    __version__ = f.read()


# Attempt to load the library
__import_c_lib = "libFSToolbox.so"

# Attempt to load the [original] C Library and expose it for python use
try:
    fstoolbox_lib = cdll.LoadLibrary(__import_c_lib)
except OSError as e:
    if not __debug__:
        print(e)
    print("Failed to load %s" % __import_c_lib)
    exit(1)


__non_discretized_argtypes = [
	c_uint,                    # k
	c_uint,                    # numOfSamples
	c_uint,                    # numOfFeatures
	POINTER(POINTER(c_uint)),  # featureMatrix 
	POINTER(c_uint),           # classColumn
	POINTER(c_uint),           # outputFeatures
	POINTER(c_double)          # featureScores
]

__discretized_argtypes = [
	c_uint,                    # k
	c_uint,                    # numOfSamples
	c_uint,                    # numOfFeatures
	POINTER(POINTER(c_uint)),  # featureMatrix 
	POINTER(c_uint),           # classColumn
	POINTER(c_double),         # outputFeatures
	POINTER(c_double)          # featureScores
]

#*******************************************************************************
#** mRMR_D() implements the minimum Relevance Maximum Redundancy criterion
#** using the difference variant, from
#**
#** "Feature Selection Based on Mutual Information: Criteria of Max-Dependency, 
# Max-Relevance, and Min-Redundancy"
#** H. Peng et al. IEEE Pattern Analysis and Machine Intelligence (PAMI) (2005)
#*******************************************************************************

mRMR_D = fstoolbox_lib.mRMR_D
mRMR_D.restype = POINTER(c_uint)
mRMR_D.argtypes = __non_discretized_argtypes

disc_mRMR_D = fstoolbox_lib.disc_mRMR_D
disc_mRMR_D.restype = POINTER(c_double)
disc_mRMR_D.argtypes = __discretized_argtypes

#*******************************************************************************
#** CMIM() implements a discrete version of the 
#** Conditional Mutual Information Maximisation criterion, using the fast
#** exact implementation from
#**
#** "Fast Binary Feature Selection using Conditional Mutual Information Maximisation"
#** F. Fleuret, JMLR (2004)
#*******************************************************************************
CMIM = fstoolbox_lib.CMIM
CMIM.restype = POINTER(c_uint)
CMIM.argtypes = __non_discretized_argtypes

discCMIM = fstoolbox_lib.discCMIM
discCMIM.restype = POINTER(c_double)
discCMIM.argtypes = __discretized_argtypes

#*******************************************************************************
#** JMI() implements the JMI criterion from
#**
#** "Data Visualization and Feature Selection: New Algorithms for Nongaussian Data"
#** H. Yang and J. Moody, NIPS (1999)
#*******************************************************************************
JMI = fstoolbox_lib.JMI
JMI.restype = POINTER(c_uint)
JMI.argtypes = __non_discretized_argtypes

discJMI = fstoolbox_lib.discJMI
discJMI.restype = POINTER(c_double)
discJMI.argtypes = __discretized_argtypes

#*******************************************************************************
#** DISR() implements the Double Input Symmetrical Relevance criterion
#** from
#** 
#** "On the Use of Variable Complementarity for Feature Selection in Cancer Classification"
#** P. Meyer and G. Bontempi, (2006)
#*******************************************************************************
DISR = fstoolbox_lib.DISR
DISR.restype = POINTER(c_uint)
DISR.argtypes = __non_discretized_argtypes

discDISR = fstoolbox_lib.discDISR
discDISR.restype = POINTER(c_double)
discDISR.argtypes = __discretized_argtypes

#*******************************************************************************
#** ICAP() implements the Interaction Capping criterion from 
#** 
#** "Machine Learning Based on Attribute Interactions"
#** A. Jakulin, PhD Thesis (2005)
#*******************************************************************************
ICAP = fstoolbox_lib.ICAP
ICAP.restype = POINTER(c_uint)
ICAP.argtypes = __non_discretized_argtypes

discICAP = fstoolbox_lib.discICAP
discICAP.restype = POINTER(c_double)
discICAP.argtypes = __discretized_argtypes

#*******************************************************************************
#** CondMI() implements the CMI criterion using a greedy forward search
#**
#** It returns an int array, not a uint array, as -1 is a sentinel value 
#** signifying there was not enough information to select a feature.
#*******************************************************************************
CondMI = fstoolbox_lib.CondMI
CondMI.restype = POINTER(c_uint)
CondMI.argtypes = __non_discretized_argtypes

CondMIICAP = fstoolbox_lib.discCondMI
CondMIICAP.restype = POINTER(c_double)
CondMIICAP.argtypes = __discretized_argtypes

#*******************************************************************************
#** MIM() implements the MIM criterion using a greedy forward search
#*******************************************************************************
MIM = fstoolbox_lib.MIM
MIM.restype = POINTER(c_uint)
MIM.argtypes = __non_discretized_argtypes

discMIM = fstoolbox_lib.discMIM
discMIM.restype = POINTER(c_double)
discMIM.argtypes = __discretized_argtypes

#*******************************************************************************
#** betaGamma() implements the Beta-Gamma space from Brown (2009).
#** This incoporates MIFS, CIFE, and CondRed.
#**
#** MIFS - "Using mutual information for selecting features in supervised neural net learning"
#** R. Battiti, IEEE Transactions on Neural Networks, 1994
#**
#** CIFE - "Conditional Infomax Learning: An Integrated Framework for Feature Extraction and Fusion"
#** D. Lin and X. Tang, European Conference on Computer Vision (2006)
#**
#** The Beta Gamma space is explained in our paper 
#** "Conditional Likelihood Maximisation: A Unifying Framework for Mutual Information Feature Selection"
#** G. Brown, A. Pocock, M.-J. Zhao, M. Lujan
#** Journal of Machine Learning Research (JMLR), 2011
#*******************************************************************************/
BetaGamma = fstoolbox_lib.BetaGamma
BetaGamma.restype = POINTER(c_uint)
BetaGamma.argtypes = __non_discretized_argtypes

discBetaGamma = fstoolbox_lib.discBetaGamma
discBetaGamma.restype = POINTER(c_double)
discBetaGamma.argtypes = __discretized_argtypes

#*******************************************************************************
#** weightedCMIM() implements a discrete version of the 
#** Conditional Mutual Information Maximisation criterion, using the fast
#** exact implementation from
#**
#** "Fast Binary Feature Selection using Conditional Mutual Information Maximisation"
#** F. Fleuret, JMLR (2004)
#*******************************************************************************
weightedCMIM = fstoolbox_lib.weightedCMIM
weightedCMIM.restype = POINTER(c_uint)
weightedCMIM.argtypes = __non_discretized_argtypes

discWeightedCMIM = fstoolbox_lib.discWeightedCMIM
discWeightedCMIM.restype = POINTER(c_double)
discWeightedCMIM.argtypes = __discretized_argtypes

#*******************************************************************************
#** WeightedJMI() implements the JMI criterion from
#**
#** "Data Visualization and Feature Selection: New Algorithms for Nongaussian Data"
#** H. Yang and J. Moody, NIPS (1999)
#*******************************************************************************
weightedJMI = fstoolbox_lib.weightedJMI
weightedJMI.restype = POINTER(c_uint)
weightedJMI.argtypes = __non_discretized_argtypes

discWeightedJMI = fstoolbox_lib.discWeightedJMI
discWeightedJMI.restype = POINTER(c_double)
discWeightedJMI.argtypes = __discretized_argtypes

#*******************************************************************************
#** weightedDISR() implements the Double Input Symmetrical Relevance criterion
#** from
#** 
#** "On the Use of Variable Complementarity for Feature Selection in Cancer Classification"
#** P. Meyer and G. Bontempi, (2006)
#*******************************************************************************
weightedDISR = fstoolbox_lib.weightedDISR
weightedDISR.restype = POINTER(c_uint)
weightedDISR.argtypes = __non_discretized_argtypes

discWeightedDISR = fstoolbox_lib.discWeightedDISR
discWeightedDISR.restype = POINTER(c_double)
discWeightedDISR.argtypes = __discretized_argtypes

#*******************************************************************************
#** weightedCondMI() implements the CMI criterion using a greedy forward search
#**
#** It returns an int array, not a uint array, as -1 is a sentinel value signifying
#** there was not enough information to select a feature.
#*******************************************************************************
weightedCondMI = fstoolbox_lib.weightedCondMI
weightedCondMI.restype = POINTER(c_uint)
weightedCondMI.argtypes = __non_discretized_argtypes

discWeightedCondMI = fstoolbox_lib.discWeightedCondMI
discWeightedCondMI.restype = POINTER(c_double)
discWeightedCondMI.argtypes = __discretized_argtypes
